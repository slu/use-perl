package My::Boilerplate;

use 5.010;
use strict;
use warnings;

sub import {
    warnings->import;
    strict->import;
}

sub unimport {
    warnings->unimport;
    strict->unimport;
}

1;
