#!/usr/bin/env perl

# use strict;

# From https://perldoc.perl.org/strict
#
#     "The strict pragma disables certain Perl expressions that could
#     behave unexpectedly or are difficult to debug, turning them into
#     errors. The effect of this pragma is limited to the current file
#     or scope block."
#


# use warnings;

# From https://perldoc.perl.org/warnings
#
#    "The warnings pragma gives control over which warnings are
#    enabled in which parts of a Perl program. It's a more flexible
#    alternative for both the command line flag -w and the equivalent
#    Perl variable, $^W.
#
#    This pragma works just like the strict pragma. This means that
#    the scope of the warning pragma is limited to the enclosing
#    block."


#
# use Modern::Perl;
#
# From https://metacpan.org/pod/Modern::Perl
#
#     "This enables the strict and warnings pragmas, as well as all of
#     the features available in Perl 5.10. It also enables C3 method
#     resolution order as documented in perldoc mro and loads IO::File
#     and IO::Handle so that you may call methods on filehandles. In
#     the future, it may include additional core modules and pragmas
#     (but is unlikely to include non-core features)."
#
# From https://perldoc.perl.org/perl5100delta
#
#     "The feature pragma is used to enable new syntax that would
#     break Perl's backwards-compatibility with older releases of the
#     language. It's a lexical pragma, like strict or warnings."
#
#     "say() is a new built-in, only available when use feature 'say'
#     is in effect..."
#
#     And much more...
#

#
# use v5.12;
#
# From https://perldoc.perl.org/perl5120delta
#
#     "Using the use VERSION syntax with a version number greater or
#     equal to 5.11.0 will lexically enable strictures just like use
#     strict would do (in addition to enabling features.)"
#

#
# use v5.36;
#
# From https://perldoc.perl.org/perl5360delta
#
#     "Furthermore, use v5.36 will also enable warnings as if you'd
#     written use warnings."
#


#
# use My::Boilerplate;
#
# From https://blogs.perl.org/users/ovid/2019/03/enforcing-simple-standards-with-one-module.html
#
#

my @x = (1,2);
print join(",",@x),"\n";

my $y = @x[1];
print "$y\n";

my $r = $y;
print "$$r\n";

print "Done!\n";
